# Browsing History Reader


Not anymore working in Marshmallow:


https://developer.android.com/about/versions/marshmallow/android-6.0-changes.html#behavior-bookmark-browser


> This release removes support for global bookmarks. The `android.provider.Browser.getAllBookmarks()` and `android.provider.Browser.saveBookmark()` methods are now removed. Likewise, the `READ_HISTORY_BOOKMARKS` and `WRITE_HISTORY_BOOKMARKS` permissions are removed. If your app targets Android 6.0 (API level 23) or higher, don't access bookmarks from the global provider or use the bookmark permissions. Instead, your app should store bookmarks data internally.


This is only available for Google Apps:


https://stackoverflow.com/questions/42502148/how-to-get-browser-history-on-android-version-23-programmatically


> This permission is limited to system apps and apps signed with Google's key.