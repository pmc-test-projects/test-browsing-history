package com.paulmark.testbrowsinghistory;

import java.util.Comparator;

/**
 * Created by jrino on 10/11/2017.
 */
public class BrowserHistory {

    private String mBrowser = "";
    private String mTitle = "";
    private String mUrl = "";
    private long mDate = -1;

    BrowserHistory(String browser, String title, String url, long date) {
        mBrowser = browser;
        mTitle = title;
        mUrl = url;
        mDate = date;
    }

    String getBrowser() {
        return mBrowser;
    }

    String getTitle() {
        return mTitle;
    }

    String getUrl() {
        return mUrl;
    }

    long getDate() {
        return mDate;
    }

    static class Comparators {
        static Comparator<BrowserHistory> DATE = new Comparator<BrowserHistory>() {
            @Override
            public int compare(BrowserHistory bh1, BrowserHistory bh2) {
                return (int) bh2.mDate - (int) bh1.mDate;
            }
        };
    }
}
