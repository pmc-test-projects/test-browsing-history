package com.paulmark.testbrowsinghistory;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author pcastillo
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<BrowserHistory> browserList = new ArrayList<>();
        List<BrowserHistory> nativeList = new ArrayList<>();
        List<BrowserHistory> chromeList = new ArrayList<>();

        Uri nativeUri = Browser.BOOKMARKS_URI;
        Uri chromeUri = Uri.parse("content://com.android.chrome.browser/history");
        Uri operaUri = Uri.parse("content://com.opera.browser/app_opera/history");

        nativeList = getBrowserHistoryList(nativeUri, "NATIVE_BROWSER");
        chromeList = getBrowserHistoryList(chromeUri, "CHROME");

        browserList.addAll(nativeList != null ? nativeList : new ArrayList<BrowserHistory>());
        browserList.addAll(chromeList != null ? chromeList : new ArrayList<BrowserHistory>());

        if (!browserList.isEmpty()) {
            Collections.sort(browserList, BrowserHistory.Comparators.DATE);
            List<String> list = new ArrayList<>();
            for (BrowserHistory history : browserList) {
                String browser = "Browser: \n" + history.getBrowser() + "\n";
                String title = "Title: \n" + history.getTitle() + "\n";
                String url = "URL: \n" + history.getUrl() + "\n";
                String date = "Date: \n" + new Date(history.getDate()).toString();
                list.add(browser + title + url + date);
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    this,
                    android.R.layout.simple_list_item_1,
                    android.R.id.text1,
                    list

            );
            ListView listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(adapter);
        }
    }

    private List<BrowserHistory> getBrowserHistoryList(Uri uri, String browser) {
        String selection = Browser.BookmarkColumns.BOOKMARK + " = 0"; // 0 = history, 1 = bookmark

        List<BrowserHistory> historyList = new ArrayList<>();
        Cursor cur = managedQuery(
                uri,
                Browser.HISTORY_PROJECTION,
                selection,
                null,
                "date DESC");
        if (cur == null) return null;
        if (cur.moveToFirst()) {
            while (!cur.isAfterLast()) {
                String title = cur.getString(Browser.HISTORY_PROJECTION_TITLE_INDEX);
                String url = cur.getString(Browser.HISTORY_PROJECTION_URL_INDEX);
                String dateStr = cur.getString(Browser.HISTORY_PROJECTION_DATE_INDEX);
                long dateLong = Long.parseLong(dateStr);
                BrowserHistory browserHistory = new BrowserHistory(browser, title, url, dateLong);
                historyList.add(browserHistory);
                cur.moveToNext();
            }
            return historyList;
        }
        return null;
    }
}
